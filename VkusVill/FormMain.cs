﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace VkusVill
{
    public partial class FormMain : Form
    {
        MySqlConnection connection;
        MySqlCommand command;

        List<Client> clients;
        List<Product> products;

        private void LoadClientsFromDB()
        {
            command.CommandText = "SELECT * FROM clients";
            clients.Clear();

            MySqlDataReader reader = command.ExecuteReader();

            while(reader.Read()==true)
            {
                int id = reader.GetInt32("id");
                string name = reader.GetString("name");
                DateTime bdate = reader.GetDateTime("bdate");

                clients.Add(new Client()
                {
                    Id = id, Name=name, BDate=bdate
                });

            }
            reader.Close();
        }
        private void FillComboBoxClients()
        {
            comboBoxClients.DataSource = clients;
            comboBoxClients.DisplayMember = "Name";
            comboBoxClients.ValueMember = "Id";
        }
        private void LoadProductsFromDB()
        {
            command.CommandText = "SELECT * FROM products";
            products.Clear();

            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read() == true)
            {
                int id = reader.GetInt32("id");
                string name = reader.GetString("name");
                decimal price = reader.GetDecimal("price");

                products.Add(new Product()
                {
                    Id = id,
                    Name = name,
                    Price = price
                });
            }
            reader.Close();
        }
        private void FillComboBoxProducts()
        {
            comboBoxProducts.DataSource = products;
            comboBoxProducts.DisplayMember = "Name";
            comboBoxProducts.ValueMember = "Id";

        }
        private void RefreshDataGridViewSales()
        {
            command.CommandText = @"SELECT s.id, s.date, c.name AS client, p.name AS product, s.amount, s.total_money FROM sales AS s JOIN clients AS c ON s.id_client=c.id
              JOIN products AS p ON s.id_product = p.id";

            dataGridViewSales.Rows.Clear();
            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read()==true)
            {
                int id = reader.GetInt32("id");
                DateTime date = reader.GetDateTime("date");
                string client = reader.GetString("client");
                string product = reader.GetString("product");
                int amount = reader.GetInt32("amount");
                decimal totalMoney = reader.GetDecimal("total_money");

                dataGridViewSales.Rows.Add(id, date, client, product, amount, totalMoney);
            }
            reader.Close();
        }

        private void ClearSalesFields()
        {
            dateTimePickerDate.Value = DateTime.Now;
            comboBoxClients.SelectedIndex = -1;
            comboBoxProducts.SelectedIndex = -1;
            numericUpDownAmount.Value = 1;
            textBoxTotalMoney.Clear();
        }

        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            string connectionString = "Server=localhost; Port=3306;  Database=vkusvill; User=root; Password=1234";
            connection = new MySqlConnection(connectionString);
            connection.Open();

            command = new MySqlCommand();
            command.Connection = connection;

            clients = new List<Client>();
            products = new List<Product>();

            RefreshDataGridViewSales();

            LoadClientsFromDB();
            FillComboBoxClients();

            LoadProductsFromDB();
            FillComboBoxProducts();

            ClearSalesFields();
        }

        private void FormMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            connection.Close();
        }

        private void numericUpDownAmount_ValueChanged(object sender, EventArgs e)
        {
            if(comboBoxProducts.SelectedItem!=null)
            {
                Product selectedProduct = (Product)comboBoxProducts.SelectedItem;

                decimal totalMoney = selectedProduct.Price * numericUpDownAmount.Value;

                textBoxTotalMoney.Text = totalMoney.ToString();
            }
        }

        private void buttonAddSell_Click(object sender, EventArgs e)
        {

            command.CommandText = $@"INSERT INTO sales(date,id_client,id_product,amount,total_money) 
              VALUES(
              '{dateTimePickerDate.Value.ToString("yyyy-MM-dd H:mm:ss")}',
              {comboBoxClients.SelectedValue},
              {comboBoxProducts.SelectedValue},
              {numericUpDownAmount.Value},
              {textBoxTotalMoney.Text.Replace(',','.')}
              )";

            command.ExecuteNonQuery();

            RefreshDataGridViewSales();
            ClearSalesFields();
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dataGridViewSales.SelectedRows.Count == 0) { return; }

            int id = int.Parse(dataGridViewSales.SelectedRows[0].Cells[0].Value.ToString());

            command.CommandText = $"DELETE FROM sales WHERE id={id}";

            command.ExecuteNonQuery();

            RefreshDataGridViewSales();
        }
    }
}
