﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VkusVill
{
    class Product
    {     
        public int Id { set; get; }
        public string Name { set; get; }
        public decimal Price { set; get; } 
    }
}
